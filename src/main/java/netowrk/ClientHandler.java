package netowrk;

import lombok.Getter;
import netowrk.packages.GameEventPackage;
import netowrk.packages.LoginPackage;
import netowrk.packages.MessagePackage;
import netowrk.packages.MovePackage;

import java.net.Socket;

public class ClientHandler extends Communicator {

	private final Server server;

	@Getter
	private final ClientPerson clientPerson;

	@Override
	protected void handlePackage(MovePackage p) {
		server.handlePackage(p);
	}

	@Override
	protected void handlePackage(MessagePackage p) {
		server.handlePackage(p, this);
	}

	@Override
	protected void handlePackage(LoginPackage p) {
		server.handlePackage(p, this);
	}

	@Override
	protected void handlePackage(GameEventPackage p) {
		server.handlePackage(p);
	}


	@Override
	protected void dealWithReceptionError() {
		server.removeNotReplyingClients();
	}

	public ClientHandler(Server server, Socket socket) {
		this.server = server;
		clientPerson = new ClientPerson(server.getUID(), "");
		super.setup(socket);
	}

	public int getId(){
		return clientPerson.getId();
	}

	public boolean isConnected() {
		return false; // TODO: implement: check if the client is still connected
	}
}