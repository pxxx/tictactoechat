package netowrk;

import game.GameEvent;
import lombok.Getter;
import netowrk.packages.*;
import netowrk.packages.Package;
import util.Notify;
import chat.Chat;
import game.Game;

import java.io.IOException;
import java.io.Serializable;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Stream;

public class Server {

	private boolean isOpenForConnections;
	private boolean isRunningGame = false;
	private int uID = -1;
	private final int port;

	private ServerSocket serverSocket;
	private final ArrayList<ClientHandler> clientHandlers = new ArrayList<>();

	@Getter
	private final Game game = new Game();

	@Getter
	private final Chat chat = new Chat();

	private final Random random = new Random();

	private int indexOfPlayer1;

	public Server(int port) {
		this.port = port;
		Notify.send(this, String.format("create server (port: %d)", port));
		startListenForConnections();
	}

	public void removeNotReplyingClients() {
		// TODO: implement: remove clients that do not reply
		Notify.send(this, "remove not replying clients");
		System.out.println("removeNotReplyingClients not implemented yet");
//		clientHandlers.forEach(handler -> {
//			if (!handler.isConnected()) {
//				System.out.printf("handler (id: %d) is not connected", handler.getId());
//				removeHandler(handler.getId());
//			}
//		});

		System.exit(1); // TODO: remove when method is implemented
	}

	public int getUID() {
		return ++uID;
	}

	public void startListenForConnections() {
		isOpenForConnections = true;
		createServerSocket();
		Notify.send(this, "start listening for client");
		while (isOpenForConnections) {
			connectClient();
			checkNumberOfPlayers();
		}

		if (isRunningGame) startGame();
	}

	private void createServerSocket() {
		try {
			serverSocket = new ServerSocket(port);
			Notify.send(this, "init server socket");
		} catch (IOException e) {
			Notify.send( this, String.format("error: failed to init server socket (port %d): %n%s", port, e));
			System.exit(1);
		}
	}

	/** checks how many players are connected and ends listening and starts game if the mux number is reached **/
	private void checkNumberOfPlayers() {
		if (getNumberOfConnectedClients() == 2) { // max. 2 players per server so stop connecting if it is reached and start the game
			stopListenForConnections();
			isRunningGame = true;
		}
	}

	public void stopListenForConnections() {
		isOpenForConnections = false;
		Notify.send(this, "stop listen for connections");
	}

	private void startGame() {
		Notify.send(this, "start game");
		game.start();
		randomInitPlayers();
		distributeMovePermission();
	}

	private void randomInitPlayers() {
		boolean b = random.nextBoolean();
		clientHandlers.get(0).send(new SetPlayerPackage(b));
		clientHandlers.get(1).send(new SetPlayerPackage(!b));
		indexOfPlayer1 = b? 0 : 1;
	}

	public void handlePackage(MovePackage p) {
		boolean isMovePossible = game.isMovePossible(p.getContent());
		if (isMovePossible) {
			game.makeMove(p.getContent());
			Notify.send(this, "granted move");
			sendPackageToAllClients(new MovePackage(p.getContent()));
			toggleMovePermission();
		}
	}

	public void handlePackage(GameEventPackage p) {
		Notify.send(this, "received Game-Event: " + p.getContent().toString());
		switch (p.getContent()) {
			case RESET -> resetGame();
			default -> Notify.send(this, "received unsupported GameEvent: " + p.getContent().toString());
		}
	}

	public void handlePackage(LoginPackage p, ClientHandler receiver) {
		receiver.getClientPerson().setUserName(p.getContent());
		Notify.send(this, String.format("log in: id: %s name: %s", receiver.getId(), receiver.getClientPerson().getUserName()));
		initChat(receiver);
		receiver.sendMessage("response to log in", this.getClass().getName());
	}

	public void handlePackage(MessagePackage p, ClientHandler receiver) {
		getChat().receiveMessage(p.getContent());
		sendPackageToAllClientsBut(receiver.getId(), p);
	}

	private void toggleMovePermission() {
		game.toggleCurrentPlayer();
		distributeMovePermission();
	}

	private void distributeMovePermission() {
		clientHandlers.get(0).send(new SetMovePermissionPackage((indexOfPlayer1 == 0) == game.getCurrentPlayer()));
		clientHandlers.get(1).send(new SetMovePermissionPackage(indexOfPlayer1 == 0 ^ game.getCurrentPlayer()));
	}

	private void connectClient() {
		Notify.send(this, "wait for connection…");

		Socket socket;
		try {
			socket = serverSocket.accept();
		} catch (IOException e) {
			Notify.send(this, "failed to connect Client");
			return;
		}

		if (!isOpenForConnections) return;
		createNewHandler(socket);
		Notify.send(this, String.format("connected Client (UID: %d)", uID));
	}

	private void close() {
		closeHandlers();
		try {
			serverSocket.close();
		} catch (IOException e) {
			Notify.displayException(e);
		}
	}

	private void createNewHandler(Socket socket) {
		ClientHandler clientHandler = new ClientHandler(this, socket);
		clientHandlers.add(clientHandler);
		clientHandler.startListening();
	}

	private void removeHandler(int id) {
		Notify.send(this, String.format("remove handler (id: %d)", id));

		ClientHandler handler = findHandlerByIdOrNull(id);
		if (handler != null) removeHandler(handler);
	}

	private void removeHandler(ClientHandler handler) {
		try {
			handler.close();
		} catch (Exception e) {
			Notify.displayException(e);
		}
		clientHandlers.remove(handler);
		Notify.send(this, "removed handler");
	}

	private ClientHandler findHandlerByIdOrNull(int id) {
		ClientHandler handler = null;
		try (Stream<ClientHandler> stream = clientHandlers.stream()) {
			Optional<ClientHandler> ch = stream.filter(h -> h.getId() == id).findFirst();
			if (ch.isPresent()) handler = ch.get();
		} catch (Exception e) {
			Notify.send(this, "no client handler with the id: " + id + " found");
		}
		return handler;
	}

	private void closeHandlers() {
		clientHandlers.forEach(ClientHandler::close);
		Notify.send(this, "closed client handlers");
	}

	public <T extends Serializable, R extends Package<T>> void sendPackageToAllClients(R p) {
		clientHandlers.forEach(handler -> handler.send(p));
	}

	public <T extends Serializable, R extends Package<T>> void sendPackageToAllClientsBut(int id, R p) {
		clientHandlers.forEach(handler -> {
			if (handler.getId() != id) handler.send(p);
		});
	}

	private int getNumberOfConnectedClients() {
		return clientHandlers.size();
	}

	public void resetGame() {
		sendPackageToAllClients(new GameEventPackage(GameEvent.RESET));
		randomInitPlayers();
		game.start();
		distributeMovePermission();
	}

	private void initChat(ClientHandler handler) {
		getChat().getChatHistory().forEach(handler::sendMessage);
	}
}