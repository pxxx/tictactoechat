package netowrk;

import lombok.Getter;

// TODO: find better name
public class ClientPerson {
	@Getter
	private final int id;
	@Getter
	private String userName;


	public ClientPerson(int id) {
		this(id, "");
	}

	public ClientPerson(int id, String userName) {
		this.id = id;
		setUserName(userName);
	}

	public void setUserName(String userName) {
		this.userName = userName.isEmpty()? String.format("%d", id) : userName;
	}
}