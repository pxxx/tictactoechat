package netowrk;

import netowrk.packages.*;
import netowrk.packages.Package;
import util.Notify;
import chat.Message;
import game.Move;

import java.io.*;
import java.net.Socket;

public abstract class Communicator extends PackageReceiver {

	protected ObjectInputStream inputStream;
	protected ObjectOutputStream outputStream;
	protected Socket socket;

	private boolean isListening = false;

	private Thread thread;

	protected void dealWithReceptionError() {
		System.exit(1);
	}

	protected void setup(Socket socket) {
		this.socket = socket;
		createStreams();
	}

	private void createStreams() {
		Notify.send(this, "create streams");
		try {
			// NOTE: the order of creation of the streams matter (first out then in)
			this.outputStream = new ObjectOutputStream(socket.getOutputStream());
			Notify.send(this, "created output-streams");
			this.inputStream = new ObjectInputStream(socket.getInputStream());
			Notify.send(this, "created input-streams");
		} catch (IOException e) {
			Notify.send(this,"error doing creation of streams" + e);
		}
	}

	public void sendLogin(String userName) {
		send(new LoginPackage(userName));
	}

	public void sendMove(Move move) {
		send(new MovePackage(move));
	}

	public void sendMessage(String context, String sender) {
		send(new MessagePackage(context, sender));
	}

	public void sendMessage(Message message) {
		send(new MessagePackage(message));
	}

	public <T extends Serializable> void send(Package<T> p) {
		Notify.send(this, "send package: " + p.getClass().getCanonicalName());
		try {
			outputStream.writeObject(p);
				Notify.send(
					this,
					String.format("sent %s", p)
				);
		} catch (IOException e) {
			Notify.send(this, "Error doing sending of package! " + e);
		}
	}

	public void startListening() {
		isListening = true;
		initThread();
		thread.start();
	}

	private void initThread() {
		thread = new Thread(() -> {
			while (isListening) {
				listen();
			}
		});
	}

	public void stopListening() {
		isListening = false;
		Notify.send(this, "stop listening");
	}

	public void close() {
		this.stopListening();
//		this.thread.stop();
		// TODO: stop thread
		if (this.thread.isAlive()) System.out.println("tread still alive");

		try {
			inputStream.close();
			outputStream.close();
			socket.close();
		} catch (IOException e) {
			Notify.send(this, "error while closing connection");
			throw new RuntimeException(e);
		}
	}

	private void listen() {
		Package<?> p = null;
		try {
			p = (Package<?>) inputStream.readObject();
		} catch (IOException | ClassNotFoundException e) {
			Notify.send(this,"error doing reception of package: "+e);
			dealWithReceptionError();
		}
		if (p != null) {
			Notify.send(this, "received package");
			handlePackage(p);
		}
	}
}