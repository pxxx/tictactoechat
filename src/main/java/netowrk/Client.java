package netowrk;

import frontend.Frontend;
import frontend.swing_frontend.GuiFrontend;
import lombok.Getter;
import netowrk.packages.*;
import util.Notify;

import java.io.IOException;
import java.net.Socket;

public class Client extends Communicator {

	private final int port;

	private final String hostName;

	@Getter
	private final String userName;

	private final Frontend frontend = new GuiFrontend(this);

	@Override
	protected void handlePackage(GameEventPackage p) {
		Notify.send(this, "received Game-Event: " + p.getContent().toString());
		switch (p.getContent()) {
			case RESET -> frontend.resetGame();
			default -> Notify.send(this, "received unsupported GameEvent: " + p.getContent().toString());
		}
	}

	@Override
	protected void handlePackage(SetMovePermissionPackage p) {
		frontend.setMovePermission(p.getContent());
	}

	@Override
	protected void handlePackage(SetPlayerPackage p) {
		frontend.setPlayer(p.getContent());
	}

	@Override
	protected void handlePackage(MovePackage p) {
		frontend.displayMove(p.getContent());
	}

	@Override
	protected void handlePackage(MessagePackage p) {
		frontend.displayMessage(p.getContent());
	}

	public Client(int port, String hostName, String userName) {
		this.port = port;
		this.hostName = hostName;
		this.userName = userName;

		connect();
	}

	private void connect() {
		super.setup(createSocket());
		this.startListening();

		login();
	}

	private Socket createSocket() {
		Socket socket = null;
		try {
			socket = new Socket(hostName, port);
			Notify.send(this, String.format("create socket (hostName: %s; port: %s)", hostName, port));
		} catch (IOException e) {
			Notify.send(
				this,
				String.format("error: failed to create socket  (hostName: %s; port: %s)", hostName, port)
			);
		}

		return socket;
	}

	private void login() {
		Notify.send(this, "log in");
		super.sendLogin(userName);
	}
}
