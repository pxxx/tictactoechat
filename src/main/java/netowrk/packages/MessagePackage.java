package netowrk.packages;

import chat.Message;

public class MessagePackage extends Package<Message> {

	public MessagePackage(Message content) {
		super(content);
	}

	public MessagePackage(String text, String sender) {
		super(new Message(text, sender));
	}
}
