package netowrk.packages;

import lombok.Getter;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
public abstract class Package<T extends Serializable> implements Serializable {
	private final T content;

	private final Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	Package(T content) {
		this.content = content;
	}
}
