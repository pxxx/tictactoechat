package netowrk.packages;

import util.Notify;

import java.io.Serializable;

public abstract class PackageReceiver {

	protected <R extends Serializable, T extends Package<R>> void handlePackage(T p) {
		Notify.send(this, "received package: " + p.getClass().getName());
		switch (p) {
			case GameEventPackage cp					-> handlePackage(cp);
			case LoginPackage cp 							-> handlePackage(cp);
			case MessagePackage cp 						-> handlePackage(cp);
			case MovePackage cp 							-> handlePackage(cp);
			case SetPlayerPackage cp 					-> handlePackage(cp);
			case SetMovePermissionPackage cp	-> handlePackage(cp);
			default -> receivedUnknownPackage(p);
		}
	}

	protected void handlePackage(GameEventPackage p) {
		receivedUnknownPackage(p);
	}

	protected void handlePackage(LoginPackage p) {
		receivedUnknownPackage(p);
	}

	protected void handlePackage(MessagePackage p) {
		receivedUnknownPackage(p);
	}

	protected void handlePackage(MovePackage p) {
		receivedUnknownPackage(p);
	}
	protected void handlePackage(SetPlayerPackage p) {
		receivedUnknownPackage(p);
	}
	protected void handlePackage(SetMovePermissionPackage p) {
		receivedUnknownPackage(p);
	}

	private <T extends Serializable> void receivedUnknownPackage(Package<T> p) {
		Notify.send(this, "no package handler implemented for:" + p.getClass().getCanonicalName());
	}
}
