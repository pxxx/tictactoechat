package chat;

import lombok.Getter;

import java.io.Serializable;
import java.sql.Timestamp;

@Getter
public class Message implements Serializable {

	private final String content;
	private final String sender;

	private final Timestamp timestamp = new Timestamp(System.currentTimeMillis());

	public Message(String content, String sender) {
		this.content = content;
		this.sender = sender;
	}
}