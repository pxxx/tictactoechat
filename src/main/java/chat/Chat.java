package chat;

import lombok.Getter;

import java.util.ArrayList;

public class Chat {

	@Getter
	private final ArrayList<Message> chatHistory = new ArrayList<>();

	public void receiveMessage(Message message) {
		chatHistory.add(message);
	}

	public void clear() {
		chatHistory.clear();
	}
}