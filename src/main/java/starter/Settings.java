package starter;

import netowrk.Client;
import netowrk.Server;

import javax.swing.UIManager;

class Settings {
	public static final int port = 6666;
	public static final String host = "localhost";

	@SuppressWarnings("SpellCheckingInspection")
	public static void startClient(String userName) {

		try { // TODO: set in GUI
			UIManager.setLookAndFeel("com.sun.java.swing.plaf.gtk.GTKLookAndFeel");
		} catch (java.lang.Exception e) {
			try { // TODO: set in GUI
				UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
			} catch (java.lang.Exception ex) {
				System.out.println("failed to set look and feel for swing");
			}
		}

		new Client(Settings.port, Settings.host, userName);
	}

	public static void startServer() {
		new Server(port);
	}
}
