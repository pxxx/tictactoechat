package util;

import java.sql.Timestamp;

public class Notify {

	public static void send(Object place, String text) {
		send(place.getClass().getCanonicalName(), text);
	}

	public static void send(String place, String text) {
		System.out.printf("[%s; %s] %s%n", place, new Timestamp(System.currentTimeMillis()), text);
	}

	public static void displayException(Exception e) {
		System.out.println(e.getClass().getName()+": "+e.getMessage());
	}
}
