package game;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@AllArgsConstructor
public class Move implements Serializable {
	@Getter
	private final Position position;
	private final boolean player;

	public Move(int x, int y, boolean player) {
		this(new Position(x, y), player);
	}

	public boolean getPlayer() {
		return player;
	}
}