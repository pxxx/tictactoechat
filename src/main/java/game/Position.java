package game;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.io.Serializable;

@Getter
public class Position implements Serializable {
	private int x;
	private int y;

	public Position(int x, int y) {
		this.setX(x);
		this.setY(y);
	}

	public void setX(int x) {
		if (x >= 0 && x <= 2) this.x = x;
		else throw new IllegalArgumentException("x-position out of field");
	}

	public void setY(int y) {
		if (y >= 0 && y <= 2) this.y = y;
		else throw new IllegalArgumentException("y-position out of field");
	}
}