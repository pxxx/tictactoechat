package game;

import lombok.Getter;

import java.util.Random;

public class Game {
	private final Random random = new Random();
	@Getter
	private boolean currentPlayer = random.nextBoolean();
	@Getter
	private final GameField field = new GameField();
	private boolean isStarted = false;

	public boolean isStarted() {
		return isStarted;
	}

	public boolean isMovePossible(Move move) {
		return move.getPlayer() == this.currentPlayer && field.isMoveAvailable(move) && isStarted;
	}

	private void reset() {
		field.reset();
		currentPlayer = random.nextBoolean();
	}

	public void toggleCurrentPlayer(){
		currentPlayer = !currentPlayer;
	}

	public void makeMove(Move move) {
		if (isMovePossible(move)) field.move(move);
	}

	public void start() {
		this.isStarted = true;
		reset();
	}

	public void stop() {
		this.isStarted = false;
	}

	public boolean getCurrentPlayer() {
		return currentPlayer;
	}
}