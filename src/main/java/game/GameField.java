package game;

import util.Notify;

public class GameField {

	/** 0 = not occupied; 1 = player 1 (false); 2 = player 2 (true) */
	private int[][] field = new int[3][3];

	GameField() {
		print();
	}

	private int getAtPosition(Position position) {
		return field[position.getX()][position.getY()];
	}

	private void setAtPosition(Position position, boolean player) {
		field[position.getX()][position.getY()] = getNumberOfBoolean(player);
	}

	public boolean isPositionEmpty(Position position) {
		return getAtPosition(position) == 0;
	}

	public boolean isMoveAvailable(Move move) {
		return isPositionEmpty(move.getPosition());
	}

	public void move(Move move) {
		if (isMoveAvailable(move)) setAtPosition(move.getPosition(), move.getPlayer());
		print();
	}
	private int getNumberOfBoolean(boolean player) { return player? 2 : 1; }

	public void reset() {
		Notify.send(this, "reset game field");
		field = new int[3][3];
		print();
	}

	private void print() {
		String s = String.format("print field:\n%s", this);
		Notify.send(this, s);
	}

	public String toString() {
		StringBuilder out = new StringBuilder("|");

		for (int x = 0; x < field.length; x++) {
			for (int y = 0; y < field[1].length; y++)
				out.append(String.format("%d|", field[x][y]));
			if (x != field.length - 1) out.append("\n-------\n|");
		}

		return out.toString();
	}
}