package frontend.swing_frontend;

import game.Move;
import game.Position;
import frontend.Frontend;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class GameGui extends JPanel {

	private final JButton[][] buttonField = new JButton[3][3];
	private final JPanel fieldPanel = new JPanel();
	private final JButton resetButton = new JButton("reset");
	private final Frontend frontend;

	public GameGui(Frontend frontend) {
		this.frontend = frontend;
		initElements();
	}

	private void initElements() {
		fieldPanel.setLayout(new GridLayout(3, 3));

		for (int x = 0; x < buttonField.length; x++)
			for (int y = 0; y < buttonField[1].length; y++) {
				buttonField[x][y] = new JButton();
				buttonField[x][y].addActionListener(createActionListener(x, y));
				fieldPanel.add(buttonField[x][y]);
			}

		this.setLayout(new BorderLayout());
		this.add(fieldPanel, BorderLayout.CENTER);
		this.add(resetButton, BorderLayout.PAGE_END);
		resetButton.addActionListener(a -> frontend.onSentResetGame());
	}

	public void resetGame() {
		setEnableButtons(false);
		clearButtons();
	}

	private void clearButtons() {
		for (JButton[] bx :buttonField)
			for (JButton b : bx)
				b.setText("");
	}

	private ActionListener createActionListener(int x, int y) {
		return al -> {
			Move move = new Move(x, y, frontend.getPlayer());
			frontend.onSendMove(move);
		};
	}

	public void displayMove(Move move) {
		getButtonAt(move.getPosition()).setText(getSymbolOfPlayer(move.getPlayer()));
	}

	private void disableButtons() {
		for (JButton[] bx : buttonField)
			for (JButton b : bx)
				b.setEnabled(false);
	}

	public void setEnableButtons(boolean isEnabled) {
		for (JButton[] bx : buttonField)
			for (JButton b : bx)
				b.setEnabled(isEnabled);
	}

	private JButton getButtonAt(Position position) {
		return buttonField[position.getX()][position.getY()];
	}

	protected String getSymbolOfPlayer(boolean player) {
		return player? "X" : "O";
	}
}