package frontend.swing_frontend;

import netowrk.Client;
import chat.Message;
import game.Move;
import frontend.Frontend;

import javax.swing.*;
import java.awt.*;

public class GuiFrontend extends Frontend {

	private final JFrame frame = new JFrame();
	private final JPanel mainPanel = new JPanel();
	private final ChatGui chatGui = new ChatGui() {
		@Override
		protected void sendTextMessage(String message) {
			onSendMessage(message);
		}
	};
	private final GameGui gameGui = new GameGui(this);

	public GuiFrontend(Client client) {
		super(client);
		initGuiElements();
	}

	@Override
	public void resetGame() {
		gameGui.resetGame();
	}

	private void initGuiElements() {
		frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
		frame.setTitle("TicTacToeChat");
		frame.add(mainPanel);
		mainPanel.setLayout(new BorderLayout());
		mainPanel.add(chatGui, BorderLayout.WEST);
		mainPanel.add(gameGui, BorderLayout.CENTER);
		frame.setSize(new Dimension(1000, 650));
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
	}

	@Override
	public void hasPermissionToMove() {
		gameGui.setEnableButtons(true);
	}

	@Override
	public void withdrawPermissionToMove() {
		gameGui.setEnableButtons(false);
	}

	@Override
	public void displayMessage(Message message) {
		chatGui.displayMessage(message);
	}

	@Override
	public void displayMove(Move move) {
		gameGui.displayMove(move);
	}
}