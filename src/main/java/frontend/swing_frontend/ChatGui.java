package frontend.swing_frontend;

import chat.Message;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public abstract class ChatGui extends JPanel {
	private final JTextField input = new JTextField();
	private final JButton sendButton = new JButton("send");
	private final JTextArea display = new JTextArea();

//	private final Frontend frontend;

//	ChatGui(Frontend frontend) {
//		this.frontend = frontend;
//		initGuiElements();
//	}

	ChatGui() {
		initGuiElements();
	}

	private void initGuiElements() {
		this.setLayout(new BorderLayout());
		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new BorderLayout());
		bottomPanel.add(input, BorderLayout.CENTER);
		bottomPanel.add(sendButton, BorderLayout.EAST);
		this.add(bottomPanel, BorderLayout.SOUTH);


		final JScrollBar horizontalScroller = new JScrollBar(JScrollBar.HORIZONTAL);
		final JScrollBar verticalScroller = new JScrollBar();
		verticalScroller.setOrientation(JScrollBar.VERTICAL);
		horizontalScroller.setMaximum (100);
		horizontalScroller.setMinimum (1);
		verticalScroller.setMaximum (100);
		verticalScroller.setMinimum (1);

		final JScrollPane scrollPane = new JScrollPane(display);
		scrollPane.setPreferredSize(new Dimension(400, 800));
		this.add(scrollPane, BorderLayout.CENTER);
		sendButton.addActionListener(al -> onSend());
		input.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				if (e.getKeyCode() == KeyEvent.VK_ENTER) onSend();
			}
		});
	}

	public void displayMessage(Message message) {
		display.append(formatMessage(message));
	}

	private String formatMessage(Message message) {
		return String.format("[%s] %s:%n%s%n%n", message.getTimestamp(), message.getSender(), message.getContent());
	}

	protected abstract void sendTextMessage(String message);

	private void onSend() {
		sendTextMessage(input.getText());
		displayMessage(new Message(input.getText(), "you"));
		input.setText("");
	}
}