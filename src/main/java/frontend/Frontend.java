package frontend;

import game.GameEvent;
import lombok.Getter;
import lombok.Setter;
import netowrk.Client;
import chat.Message;
import game.Move;
import netowrk.packages.GameEventPackage;

public abstract class Frontend {

	@Getter @Setter
	private boolean player;
	private boolean isMoveTime = false;
	private final Client client;

	protected Frontend(Client client) {
		this.client = client;
	}

	public abstract void resetGame();
	public void onSentResetGame(){
		client.send(new GameEventPackage(GameEvent.RESET));
	}

 	public void onSendMove(Move move) {
		client.sendMove(move);
	}
	public void onSendMessage(String text) {
		client.sendMessage(text, client.getUserName());
	}

	public abstract void hasPermissionToMove();
	public abstract void withdrawPermissionToMove();

	public abstract void displayMessage(Message message);

	public abstract void displayMove(Move move);

	public void setMovePermission(boolean haseMovePermission) {
		this.isMoveTime = haseMovePermission;
		if (haseMovePermission) hasPermissionToMove();
		else withdrawPermissionToMove();
	}

	public boolean isMoveTime() {
		return isMoveTime;
	}

	// TODO: implement what to do on exit

	public boolean getPlayer() {
		return player;
	}
}
