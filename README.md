# TicTacToeChat

A TicTacToe implementation with that you can play over the network, and it has a chat.


Note that this project is just a prototype and not fully worked out yet. It is just an attempt of me to create a game that can be played over the network.
At the moment the chat works, and you can play and restart the game. But some things are not implemented yet like for example that the game notices when someone wins.
Also, the Server and the clients crash if either one of them is closed, losses connection or crashes.



![screen-shot](./images/Screenshot_2022-06-07_23-52-39.png)
